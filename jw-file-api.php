<?php
namespace Grav\Plugin;

use Exception;
use Grav\Common\Plugin;

include 'helpers/jw-serve-file.php';
include 'helpers/markup.php';
include 'helpers/uploader.php';
include 'helpers/downloader.php';
include 'helpers/misc.php';

// I want to work in SCSS. Don't like working in plain CSS.
require 'scssphp/scss.inc.php';
use Leafo\ScssPhp\Compiler;

/**
 * Class JwFileApiPlugin
 * @package Grav\Plugin
 */
class JwFileApiPlugin extends Plugin
{
    const IMGEXT = ['tif', 'tiff', 'jpg', 'jpeg', 'png', 'bmp', 'gif', 'webp'];

    /**
     * @return array
     *
     * The getSubscribedEvents() gives the core a list of events
     *     that the plugin wants to listen to. The key of each
     *     array section is the event that the plugin listens to
     *     and the value (in the form of an array) contains the
     *     callable (or function) as well as the priority. The
     *     higher the number the higher the priority.
     */
    public static function getSubscribedEvents()
    {
        return [
            'onPluginsInitialized' => ['onPluginsInitialized', 0],
            'onTwigInitialized' => ['onTwigInitialized', 0]
        ];
    }

    /**
     * Initialize the plugin
     */
    public function onPluginsInitialized()
    {
        // Don't proceed if we are in the admin plugin
        if ($this->isAdmin()) {
            return;
        }

        // Enable the main event we are interested in
        $this->enable([
            'onPageInitialized' => ['onPageInitialized', 0]
        ]);
    }


    public function onTwigInitialized() {
        $this->grav['twig']->twig()->addFunction(
            new \Twig_SimpleFunction('injectFileSysHTML', [$this, 'injectFileSysHTML'])
        );
    }

    function injectFileSysHTML() {
        $this->compileSCSS();

        
        $this->grav['assets']->addJs('/user/plugins/jw-file-api/js/jf-injection.js');
        $this->grav['assets']->addJs('/user/plugins/jw-file-api/js/jf-createmarkup.js');
        $this->grav['assets']->addJs('/user/plugins/jw-file-api/js/jf-sortcontent.js');
        $this->grav['assets']->addJs('/user/plugins/jw-file-api/js/jf-treeview.js');
        $this->grav['assets']->addJs('/user/plugins/jw-file-api/js/jf-deleteitem.js');
        $this->grav['assets']->addJs('/user/plugins/jw-file-api/js/jf-modal.js');
        $this->grav['assets']->addJs('/user/plugins/jw-file-api/js/jf-download.js');
        $this->grav['assets']->addJs('/user/plugins/jw-file-api/js/jf-newfolder.js');
        $this->grav['assets']->addCss('/user/plugins/jw-file-api/css/base.css');
        $this->grav['assets']->addCss('/user/plugins/jw-file-api/css/results.css');
        $this->grav['assets']->addCss('/user/plugins/jw-file-api/css/modal.css');

        addModalOverlay($this->grav["uri"]->rootURL(true));
        makeMarkup($this->grav["uri"]->rootURL(true), $this->grav['uri']->path());
    }


    // This is essentially our main router. It routes.
    public function onPageInitialized() {
        $basepath = $_SERVER['DOCUMENT_ROOT'] . $this->grav['uri']->rootURL() . "/user/files/";
        switch ($this->grav['uri']->path()) {
            case "/showfiles":
                try {
                    $this->checkPrivileges();
                    $this->displayContent($_GET['path']);
                } catch (\Exception $e) {
                    echo $e->getMessage();
                }
                exit();
                break;

            case "/serve-json":
                try {
                    $this->checkPrivileges();
                    $filepath = "";
                    if(\array_key_exists("file", $_GET)) {
                        $filepath = __DIR__ . "\\" . $_GET['file'];
                    } else {
                        header('HTTP/1.0 404 Not Found');
                        echo "Huch, hier ist nichts!";
                    }
                    $data = \file_get_contents($filepath);
                    header('Content-Type: application/json');
                    echo $data;
                } catch (\Throwable $th) {
                    header('HTTP/1.0 403 Forbidden');
                    echo "<div><h1>FEHLER</h1>Keine Berechtigung oder fehlerhafter Pfad!</div>";
                }
                exit();
                break;

            case "/serve-file":
                try {
                    $this->checkPrivileges();
                    $filepath = "";
                    if(\array_key_exists('file', $_GET)) {
                        $pathparts = explode('/', $_GET['file']);
                        if(count($pathparts) > 1) {
                            // This is for temporary zip files. We just want to know if the asking client is allowed to download the file.
                            // If he's not (determined by comparing the hash of his username to the hash in front of the file) we just 403.
                            if($pathparts[0] === '_tmp' || $pathparts[1] === '_tmp') {
                                $userh = hash("md5", $this->grav['user']['username']);
                                $fileh = explode("_", pathinfo($_GET['file'])['basename'])[0];
                                if($userh !== $fileh) {
                                    header('HTTP/1.0 403 Forbidden');
                                    exit('Not allowed.');
                                }
                                $filepath = $basepath . $_GET['file'];
                                download($filepath);
                                sleep(3);
                                unlink($filepath);
                                exit();
                            }
                        }
                        $filepath = $basepath . $_GET['file'];
                    } else {
                        header('HTTP/1.0 404 Not Found');
                        echo "Huch, hier ist nichts!";
                        exit();
                    }
                    $data = \file_get_contents($filepath);
                    header('Content-Type:' . mime_content_type($filepath));
                    echo $data;
                    exit();
                } catch (\Throwable $th) {
                    header('HTTP/1.0 403 Forbidden');
                    echo 'Not allowed.';
                }
                exit();
                break;

            case "/rsfile":
                try {
                    $this->checkPrivileges();
                    $filepath = "";
                    if(\array_key_exists('file', $_GET)) {
                        $filepath = $basepath . $_GET['file'];
                    } elseif (\array_key_exists("file", $_POST)) {
                        $filepath = $basepath . $_POST['file'];
                    } else {
                        \header('HTTP/1.0 404 Not Found');
                        echo "Huch, hier ist nichts!";
                        exit();
                    }
                    generateFileView(\preg_replace('#/+#', '/', $filepath));
                    exit();
                } catch (\Throwable $th) {
                    header('HTTP/1.0 403 Forbidden');
                    echo "<div><h1>FEHLER</h1>Keine Berechtigung oder fehlerhafter Pfad!</div>";
                }
                exit();
                break;

            case "/thumbnail":
                try {
                    $this->checkPrivileges();
                    $filepath = "";
                    if($_GET['file']) {
                        $filepath = $basepath . $_GET['file'];
                    } else {
                        header('HTTP/1.0 400 Bad Request');
                        throw new Exception("Bad Request.", 1);
                    }

                    if(!in_array(pathinfo($filepath)['extension'], JwFileApiPlugin::IMGEXT) || !file_exists($filepath)) {
                        header('HTTP/1.0 404 Not Found');
                        throw new Exception("Datei nicht gefunden.", 1);
                    }

                    // $data = \file_get_contents($filepath);
                    $data = createThumbnail($filepath, 200, 200);
                    header('Content-Type: image/jpeg');
                    echo $data;
                } catch (\Exception $e) {
                    echo ($e->getMessage());
                }
                exit();
                break;

            // Generate TreeView data
            case "/treeview":
                try {
                    $this->checkPrivileges();
                    $folderstructure = [];
                    $folderstructure['directories'] = $this->cascadingScandir($basepath);
                    header("Content-Type: application/json");
                    echo json_encode($folderstructure);
                } catch (\Exception $e) {
                    echo ($e->getMessage());
                }
                exit();
                break;

            // New folder
            case "/jnf":
                try {
                    $this->checkPrivileges();
                    if(!\array_key_exists("path", $_GET)) {
                        throw new Exception("Wrong. Just wrong.", 1);
                    }
                    $dirpath = preg_replace('#/+#','/', str_replace("/..", "/", ($basepath . \str_replace("/files", "", $_GET['path']))));
                    if(\file_exists($dirpath)) {
                        header("HTTP/1.0 400 Bad Request");
                        echo "Das Verzeichnis existiert bereits!";
                    } else {
                        if(\mkdir($dirpath, 0666)) {
                            header("HTTP/1.1 200 OK");
                            echo "Das Verzeichnis wurde erstellt.";
                        } else {
                            header("HTTP/1.0 500 Internal Server Error");
                            throw new Exception("Verzeichnis konnte nicht erstellt werden.", 1);
                        }
                    }
                } catch (\Exception $e) {
                    echo ($e->getMessage());
                }
                exit();
                break;

            // Helper function: Deletes all .json-files in specified folder (has to be subdirectory of /user/files, of course).
            case "/flushjson":
                try {
                    $this->checkPrivileges();
                    if(!array_key_exists("path", $_GET)) {
                        throw new Exception("Error Processing Request", 1);
                    }
                    $dir = \preg_replace('#/+#', '/', (\str_replace("/files", "", $basepath) . $_GET['path']));
                    echo $dir;

                    $files = \array_diff(\scandir($dir), array(".", ".."));
                    foreach ($files AS $entry) {    
                        if(\array_key_exists('extension', \pathinfo($entry))) {
                            if(\pathinfo($entry)['extension'] === "json") {
                                \unlink($dir . "/" . $entry);
                            }
                        }
                    }
                } catch (\Exception $e) {
                    header("HTTP/1.0 500 Internal Server Error");
                    echo "Something went wrong!";
                }
                exit();
                break;
            
            // Deletes files/folders from the /user/files structure.
            case "/del":
                try {
                    $this->checkPrivileges();
                    if(!\array_key_exists("files", $_POST)) {
                        header("HTTP/1.0 500 Bad Request");
                        throw new Exception("Gib mir Files zum Deleten!", 1);
                    }

                    foreach ($_POST['files'] AS $entry) {
                        $path = \preg_replace('#/+#', '/', \str_replace("files/", "", $basepath) . \urldecode($entry));
                        if(\file_exists($path)) {
                            if(\is_dir($path)) {
                                $json = \pathinfo($path)['dirname'] . "/" . \pathinfo($path)['filename'] . ".json";
                                try {
                                    echo (\rmdir($path) ? "Das Verzeichnis wurde gelöscht." : "Das Verzeichnis konnte NICHT gelöscht werden!");
                                    echo (\unlink($json) ? "Die zum Verzeichnis gehörende .json-Datei wurde gelöscht." : "Die zum Verzeichnis gehörende .json-Datei konnte NICHT gelöscht werden!");
                                } catch (\Exception $e) {
                                    echo $e->getMessage();
                                    // echo "The directory " . $path . " could not be deleted!";
                                }
                            } else {
                                $json = $path . ".json";
                                echo (\unlink($path) ? "Die Datei " . $path ." wurde gelöscht." : "Die Datei " . $path . " konnte NICHT gelöscht werden!");
                                echo (\unlink($json) ? "Die zur Datei " . $path ." gehörende .json-Datei wurde gelöscht." : "Die zur Datei " . $path . " gehörende .json-Datei konnte NICHT gelöscht werden!");
                            }
                        } else {
                            echo "Der Pfad " . $path . " existiert nicht!";
                        }
                    }
                } catch (\Exception $e) {
                    echo $e->getMessage();
                }
                exit();
                break;
            case "/jw-file-upload":
                try {
                    $this->checkPrivileges();
                    $errors = array();

                    if(!isset($_GET['path'])) {
                        throw new Exception("Bitte einen validen Pfad angeben!", 1);
                    }
                    $res = uploadFiles($basepath, $_GET['path'], $_FILES);
                    // if()
                    echo "Die Dateien wurden hochgeladen!";
                } catch (\Exception $e) {
                    header("HTTP/1.0 500 Bad Request");
                    echo 'Fehler! ' . $e->getMessage();
                }
                exit();
                break;

            case "/jf-file-download":
                try {
                    $this->checkPrivileges();
                    if(!isset($_POST['files'])) {
                        throw new Exception("Bad Request. No, wait... actually *worst* request. Stop doing that!", 1);
                    }
                    downloadFiles($basepath, $_POST['files'], hash("md5", $this->grav['user']['username']));
                } catch (\Throwable $e) {
                    echo 'Fehler! ' . $e->getMessage();
                }
                exit();
                break;
            }
    }



    function checkPrivileges() {
        if($this->grav['user']['authorized'] !== true) {
            header("HTTP/1.0 403 Forbidden");
            echo "Keine ausreichende Berechtigung.";
            exit();
        }
    }

    function compileSCSS() {
        $base = $_SERVER['DOCUMENT_ROOT'] . $this->grav['uri']->rootURL();
        $scss = new Compiler();
        $scss_folder = $base . "/user/plugins/jw-file-api/scss/";
        $css_folder = $base . "/user/plugins/jw-file-api/css/";

        $filelist = glob($scss_folder . "*.scss");

        foreach ($filelist AS $file_path) {
            $file_path_elements = pathinfo($file_path);
            $file_name = $file_path_elements['filename'];
            $string_sass = file_get_contents($scss_folder . $file_name . ".scss");
            $string_css = $scss->compile($string_sass);
            file_put_contents($css_folder . $file_name . ".css", $string_css);
        }
    }


    function cascadingScandir($path) {
        $folders = [];
        $all = array_diff(scandir($path), array(".", ".."));
        if(!empty($all)) {
            foreach ($all as $entry) {
                if(is_dir($path . "/" . $entry) && $entry !== "_tmp") {
                    // We construct a link by replacing the actual address on the hard
                    // drive by our domain name and cutting out Grav's "/user" folder.
                    // Yes, there are more elegant ways to do this, but this works just fine, so... ¯\_(ツ)_/¯
                    $link = str_replace("/user", "", str_replace($_SERVER['DOCUMENT_ROOT'], $this->grav['uri']->base(), preg_replace('#/+#', '/', ($path . "/" . $entry))));
                    $subentry = [];
                    $subentry["name"] = $entry;
                    $subentry["link"] = $link;
                    $subentry["directories"] = $this->cascadingScandir($path . "/" . $entry);
                    $folders[] = $subentry;
                }
            }
        }
        return $folders;
    }

    function displayContent($p) {
        // Alles, was wir machen, findet im Verzeichnis /user/files statt.
        $path = "./user" . $p . "/";
        if($p === "/") {
            $path = "./user/files/";
        }

        if(is_dir($path)) {
            $entries = array_diff(scandir($path), array(".", ".."));
        } else {
            $entries = "Verzeichnis ist inexistent.";
        }
        
        // Wir filtern alle .json-Dateien sowie das _tmp-Verzeichnis heraus.
        $tmp = array();
        foreach($entries AS $key => $entry) {
            if(!is_dir($path . $entry)) {
                if(pathinfo($entry)['extension'] !== 'json') {
                    $tmp[] = $entry;
                }
            } else {
                if(pathinfo($entry)['basename'] !== '_tmp') {
                    $tmp[] = $entry;
                }
            }
        }
        $entries = $tmp;

        $ret = array();
        foreach($entries AS $entry) {
            $x = $this->checkJSON($path, $entry);
            if($x !== null) {
                $ret[] = $x;
            }
        }
        header('Content-Type: application/json');
        echo json_encode($ret, JSON_PRETTY_PRINT);
    }

    /**
     * Looks for a corresponding json file to the given path/entry (which can be both a file or a directory).
     * If none exists, a blank file is created and filled with generic information.
     */
    function checkJSON($path, $entry) {
        try {
            if(!is_dir($path . $entry)) {
                if(!file_exists($_SERVER['DOCUMENT_ROOT'] . $this->grav['uri']->rootURL() . substr(($path . $entry), 1) . ".json")) {
                    $yfile = $path . $entry . ".json";
                    $handle = fopen($yfile, 'w') or die('Cannot open file: ' . $yfile);
                    $filedata['name'] = \pathinfo($entry)['filename'];
                    $filedata['type'] = \pathinfo($entry)['extension'];
                    $filedata['path'] = rawurlencode(\preg_replace('#/+#', '/', \substr(($path . $entry), 6)));
                    $filedata['desc'] = "[Keine Beschreibung vorhanden.]";
                    $filedata['tags'] = array(["File"]);
                    $filedata['comments'] = array();
    
                    fwrite($handle, json_encode($filedata, JSON_PRETTY_PRINT));
                    fclose($handle);
                    return $filedata;
                } else {
                    $file = json_decode(file_get_contents(($path . $entry) . ".json"), true);
                    return $file;
                }
            } else {
                if(!file_exists($_SERVER['DOCUMENT_ROOT'] . $this->grav['uri']->rootURL() . substr(($path . $entry), 1) . ".json")) {
                    $yfile = $path . $entry . ".json";
                    $handle = fopen($yfile, 'w') or die('Cannot open file: ' . $yfile);
                    $filedata['name'] = $entry;
                    $filedata['type'] = "directory";
                    $filedata['path'] = \rawurlencode(preg_replace('#/+#', '/', substr(($path . $entry), 6)));
                    $filedata['desc'] = "[Keine Beschreibung vorhanden.]";
                    $filedata['tags'] = array(["Directory"]);
                    $filedata['comments'] = array();
    
                    fwrite($handle, json_encode($filedata, JSON_PRETTY_PRINT));
                    fclose($handle);
                    return $filedata;
                } else {
                    $file = json_decode(file_get_contents(($path . $entry) . ".json"), true);
                    return $file;
                }
            }
        } catch (\Exception $e) {
            return null;
        }
    }
}
?>
