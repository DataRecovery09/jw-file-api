function jfOpenModal(cause, ) {
    jQuery('#jf-modal-overlay').css("display", "block");
}

function jfCloseModal() {
    jQuery('#jf-modal-overlay').css("display", "none");
}

jQuery(document).ready(function() {
    window.onclick = function(event) {
        if(event.target == this.document.getElementById("jf-modal-overlay")) {
            this.jfCloseModal();
        }
    }
})
