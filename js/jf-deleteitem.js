let jfDeleteItem = async function(basepath) {
    console.log(jMarkedtiles)
    let ajpayload = {};
    ajpayload['files'] = jMarkedtiles;

    let startDelete = new Promise((resolve, reject) => {
        jQuery.ajax({
            type:'POST',
            url: basepath + '/del',
            data: ajpayload,
            success: function(response) {
                console.log(response);
                resolve(response);
            },
            error: function(response) {
                console.error(response);
                reject(response);
            }
        });
    });
    let res = await startDelete;
    // jfGetContent(jCurrentDir, basepath);
    // jfCreateTreeview(basepath);
};
