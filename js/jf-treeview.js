let toggleTreeBranch = function(e) {
    let ele = e.target.parentElement;
    if(e.target.dataset.openlist === "true") {
        let list = jQuery(ele).find('ul:first');
        jQuery(list).css("display", "none");
        e.target.dataset.openlist = "false";
        jQuery(e.target).removeClass('rot-down');
        jQuery(e.target).addClass('rot-right');
    } else {
        let list = jQuery(ele).find('ul:first');
        jQuery(list).css("display", "block");
        e.target.dataset.openlist = "true";
        jQuery(e.target).removeClass('rot-right');
        jQuery(e.target).addClass('rot-down');
    }

};

let jfHighlightCurrentBranch = function(currentPath) {
    jQuery('.treeview-link').removeClass('treeview-active');
    jQuery('a[href="' + currentPath + '"').each((_i, element) => {
        jQuery(element).addClass('treeview-active');
    })
};


async function createTreeview(baseurl) {
    jQuery(".treeview-rootlist").remove();
    let getTreeview = new Promise((resolve, reject) => {
        jQuery.ajax({
            type:'get',
            url: baseurl + '/treeview',
            success:function(response) {
                resolve(response);
            },
            error: function(response) {
                console.error(response);
                displayError();
                reject(response);
            }
        });
    });
    let tree = await getTreeview;
    tree.link = baseurl + "/files";
    tree.name = "Files";
    
    let recursiveTreeview = function (obj) {
        let sublist = "";
        let htmele = "<li class='treeview-sublist-container'>";
        let hasChildren = false;
        if(obj['directories'].length > 0) {
            hasChildren = true;
        }
        if(hasChildren) {
            htmele += "<i class='fa fa-caret-down rot-down treeview-ctrl' data-openlist='true'></i>";
        } else {
            htmele += "<i class='fa fa-caret-down rot-right treeview-ctrl' data-openlist='false'></i>";
        }
        htmele +=   "<a class='treeview-link' href='"
                    + encodeURI(obj.link)
                    + "'>" + obj.name + "</a>";
        if(hasChildren) {
            htmele += "<ul>";
            obj['directories'].forEach(element => {
                sublist = recursiveTreeview(element);
                htmele += sublist;
            });
            htmele += "</ul>";
        }
        htmele += "</li>";
        return htmele;
    };
    let res = recursiveTreeview(tree);
    let froot = "<ul class='treeview-rootlist'>" + res + "</ul>";
    let target = jQuery("#treeview-container");
    target.append(froot);

    jQuery(".treeview-ctrl").on("click", (e) => {
        toggleTreeBranch(e);
    });

    jQuery(".treeview-link").click((event) => {
        event.preventDefault();
        let ajlink = (event.target.href).replace(jfroot, "");
        jQuery('.result-tile').remove();
        getContent(ajlink, jfroot);
    });
    jfHighlightCurrentBranch(jCurrentDir);
}
