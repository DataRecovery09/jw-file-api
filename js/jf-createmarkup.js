async function getContent(path, baseurl) {
    jMarkedtiles.length = 0;    // Empty the array of marked tiles.
    jCurrentDir = path;         // In which Directory are we right now?

    jQuery('.result-tile').remove();
    jwcurrpath = baseurl + path;
    jfHighlightCurrentBranch(jwcurrpath);
    
    let getDictionary = new Promise((resolve, reject) => {
        jQuery.ajax({
            type:'get',
            url: baseurl + '/serve-json?file=fadictionary.json',
            success:function(response) {
                resolve(response);
            },
            error: function(response) {
                console.error(response);
                displayError();
                reject(response);
            }
        });
    });
    let dict = await getDictionary;

    let showFiles = new Promise((resolve, reject) => {
        if(filerequest !== null) {
            filerequest.abort();
        }

        filerequest = jQuery.ajax({
            type:'get',
            url: baseurl + '/showfiles?path=' + path,
            // data:{'path': path},
            success:function(response) {
                resolve(response);
            },
            error: function(response) {
                console.error(response);
                reject(response);
            }
        });
    }); 
    let res = await showFiles;
    filerequest = null;
    injectContent(res, dict);
    
    // In case we receive an error we let the user know that something bad happened. No Waiting for Godot.
    let displayError = function(error) {
    };

    jfSortContent("type");
    jfMakeTileLinks();

    // Checkboxes/marking of tiles
    jQuery(".jf-tile-selector").change((event) => {
        let tile = event.target.parentElement.parentElement;
        if(event.target.checked) {
            if(!jMarkedtiles.includes(tile.dataset.path)) {
                jMarkedtiles.push(tile.dataset.path);
            }
        } else {
            let idx = jMarkedtiles.indexOf(tile.dataset.path);
            if(idx !== -1) {
                jMarkedtiles.splice(idx, 1);
            }
        }
    });    
}


let injectContent = function (data, dict) {
    let target = jQuery('#ajax-results');

    let constructTile = function(data) {
        let link;
        if(data.type === "directory") {
            link = "<div class='result-tile directory' "
            + constructMeta(data)
            + ">"
            + constructMarking()
            + "<a class='tile-link' href='" + jfroot + decodeURIComponent(data.path) + "'>";
        } else {
            let href = jfroot + decodeURIComponent(data.path);
            let arr = href.split("/");
            arr[arr.indexOf('files')] = "rsfile?file=";
            href = arr.join("/");
            enchref = href.replace(/ /g, "%20");
            link = "<div class='result-tile file' "
                    + constructMeta(data)
                    + ">"
                    + constructMarking()
                    + "<a href='" + enchref + "' data-rel='lightcase:tiles'>";
        }
    
        let tile = "<div><div class='icon-cont'>"
                + constructThumbnail(data.type, data.name)
                + constructIcon(data.type)
                + "</div>"
                + constructName(data.name, data.type)
                + "</div>";
        return link + tile + "</a></div>";
    };
    
    let constructMeta = function(data) {
        let meta = "";
        let type = "data-type='" + data.type + "' ";
        let name = "data-name='" + data.name + "' ";
        let path = "data-path='" + data.path + "' ";
        meta += type + name + path;
        return meta;
    }
    
    let constructName = function(name, type) {
        let n = "<h6 class='result-tile-title'>" + name;
        if(type !== "directory") {
            n += "." + type;
        }
        n += "</h6>";
        return n;
    };
    
    let constructThumbnail = function(type, name) {
        let imgnode = "";
        if(type === "jpg" || type === "jpeg" || type === "png") {
            let imglink = jfroot + jCurrentDir + "/" + name + "." + type;
            let arr = imglink.split("/");
            arr[arr.indexOf('files')] = "thumbnail?file=";
            imglink = arr.join("/");
            imgnode = "<img class='img-thmb' src='" + imglink + "'/>";
        }
        return imgnode;
    };
    
    let constructIcon = function(icon) {
        let i = "<i class='fa fa-file-o'></i>";
        dict.forEach(obj => {
            if(Object.entries(obj)[0][1].includes(icon.toLowerCase())) {
                let fa = Object.entries(obj)[0][0];
                i = "<i class='fa fa-" + fa + "'></i>";
                return i;
            }
        });
        return i;
    };
    
    let constructMarking = function() {
        let m = "<label class='tile-marking'><input type=\"checkbox\" name=\"marked\" value=\"Markiert\" class=\"jf-tile-selector\"></input><span class='checkmark'></span></label>";
        return m;
    }

    data.forEach(el => {
        let markup = constructTile(el);
        target.append(markup);
    });
};


function jfMakeTileLinks() {
    jQuery(".tile-link").click((event) => {
        jQuery('.result-tile').remove();
        event.preventDefault();
        let ajlink = (event.currentTarget.href).replace(jfroot, "");
        getContent(ajlink, jfroot);
        jfHighlightCurrentBranch(jCurrentDir);
    });
}
