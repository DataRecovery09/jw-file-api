let jCurrentDir = "";
let jMarkedtiles = [];
let filerequest = null;

function jFileSystem(p, b) {

    // Description: Call to an open API to retrieve the corresponding entries in the file system.
    // The API returns an array of json-encoded objects. Each of the entries is used to construct
    // a pretty tile with some information... kind of like the windows explorer, just better suited
    // for the specific needs of the WFV-Info. ¯\_(ツ)_/¯
    jQuery(document).ready(()=> {
        jQuery('#btn-newfolder').click((event)=> {
            let nfres = window.prompt("Neuen Ordner erstellen", "Neuer Ordner");
            if(nfres !== null) {
                jNewFolder(nfres);
            }
        });
        jQuery('#upload-submit').click((event) => {
            event.preventDefault();
            let upfiles = document.querySelector('[type=file]').files;
            let formData = new FormData();

            for (i = 0; i < upfiles.length; i++) {
                let file = upfiles[i];
                formData.append(file.name, file, file.name);
            }

            let clearFileInput = function(f) {
                try {
                    f.value = '';    
                } catch (err) {
                    console.error(err);
                }
            };

            jQuery.ajax({
                url: b + "/jw-file-upload?path=/" + jCurrentDir,
                type: "POST",
                data: formData,
                processData: false,
                cache: false,
                contentType: false,
                xhr: function() {
                    let xhr = new window.XMLHttpRequest();
                    xhr.upload.addEventListener("progress", function(e) {
                        console.log(e);
                        if(e.lengthComputable) {
                            let percent = e.loaded / e.total;
                            console.log(percent);
                        }
                    }, false);
                    return xhr;
                },
                success: response => {
                    console.log(response);
                    getContent(jCurrentDir, b);
                    // clearFileInput(document.querySelector('[type=file]'));
                },
                error: response => {
                    jfOpenModal();
                    console.log(response);
                }
            });
        });
        jQuery('#btn-delete').click((_e) => {
            jfDeleteItem(b);
            getContent(jCurrentDir, b);
            createTreeview(b);
        });

        /**
         * BUG 0: If all the tiles are deleted, the select-all-mark has to be resetted.
         */
        jQuery('#btn-select-all').click((event) => {
            // console.log(event.target.checked);
            if(event.target.checked) {
                jQuery('.jf-tile-selector').each((_idx, el) => {
                    if(!el.checked) {
                        el.click();
                    }
                });
            } else {
                jQuery('.jf-tile-selector').each((_idx, el) => {
                    if(el.checked) {
                    el.click();
                    }
                });
            }
        });
    });

    getContent(p, b);
    createTreeview(jfroot);
}
