/**
 * SORTIERUNG
 * ----------
 * para => Nach was sortiert wird.
 * inv  => Invertiert die Sortierung, falls true.
 * 
 * Wir zerlegen die Einträge entsprechend des Sortier-Parameters in mehrere
 * kleinere Key-Value-Objekte und sortieren sie alphabetisch. Die
 * resultierenden Arrays werden jeweils in eine Key=>Value-Map gesteckt,
 * die dann ebenfalls nach ihren Keys sortiert wird.
 */

function jfSortContent(para, inv = false) {
    // Überlegung: Wenn vor dem Sortieren alle versteckten Einträge rausgefiltert werden,
    // verringert sich dadurch der Overhead durch die Sortierung selbst. Andererseits
    // muss der Browser dann jedesmal, wenn Elemente versteckt/enthüllt werden, erneut
    // sortieren. Was besser ist, hängt vmtl. vom Nutzungsprofil der jeweiligen Funktion
    // ab. Das wird sich erst mit der Zeit zeigen - sollte es überhaupt eine Rolle spielen...
    let tiles = jQuery('.result-tile:not(.hidden)');
    let files = jQuery('.file:not(.hidden)');
    let dirs = jQuery('.directory:not(.hidden)');

    let resobj = {};

    switch (para) {
        case "type":
            typ: {
                let tempobj = {};
                let types = [];
                jQuery.each(tiles, function(x, val) {
                    let ty = val.dataset.type;
                    if(!types.includes(ty) && ty !== 'directory') {
                        types.push(ty);
                    }
                    if(!tempobj[ty]) {
                        let arr = [];
                        tempobj[ty] = arr;
                    }
                    tempobj[ty].push(val);
                });
                types.sort();
                if(inv) {
                    types.reverse();
                }
    
                resobj['directory'] = tempobj['directory'];
                types.forEach(el => {
                    resobj[el] = tempobj[el].sort();
                });
            }
            break;

        case "name":
            name: {
                resobj['directories'] = [];
                resobj['files'] = [];
                let fileobj = {};
                let filenamearr = [];
                let dirobj = {};
                let dirnamearr = [];
                jQuery.each(files, function(x, val) {
                    let name = val.dataset.name + "." + val.dataset.type;
                    fileobj[name] = val;
                    filenamearr.push(name);
                });
                jQuery.each(dirs, function(x, val) {
                    let name = val.dataset.name;
                    dirobj[name] = val;
                    dirnamearr.push(name);
                });
                filenamearr.sort();
                dirnamearr.sort();
                if(inv) {
                    filenamearr.reverse();
                    dirnamearr.reverse();
                }
                dirnamearr.forEach(entry => {
                    resobj['directories'].push(dirobj[entry]);
                });
                filenamearr.forEach(entry => {
                    resobj['files'].push(fileobj[entry]);
                });
            }
            break;

        case "date-created":
            console.log("Sortierung: Datum0.");
            break;

        case "date-modified":
            console.log("Sortierung: Datum1.");
            break;

        case "favs":
            console.log("Sortierung: Favs.");
            break;

        default:
            console.log("Sortierung: Def.");
            break;
    }
    

    // Remove the tiles, then re-add them in the correct order from resobj.
    jQuery('.result-tile').remove();
    for(el in resobj) {
        console.log(resobj[el])
        if(resobj[el]) {
            resobj[el].forEach(entry => {
                jQuery('#ajax-results').append(entry)
            })
        }
    }

    lightcase: {
        // Durch das Sortieren werden die Lightcase-Event-Listener verkaputtiert, also
        // werden sie nach dem Sortiervorgang erneut gesetzt.
        jQuery('a[data-rel^=lightcase]').lightcase();
            
        // Lightcase.js hat die nervige Angewohnheit, die Scrollbar des Dokuments auszublenden.
        // Ich kann das nicht leiden; u. a., weil es den Content hin- und herspringen lässt.
        // Lösung: Wir binden ein Event an die Lightcase-Links und blenden die Scrollbar einfach
        // wieder ein. 1:0 für mich, Lightcase! ( ͡° ͜ʖ ͡°)
        jQuery('a[data-rel^=lightcase]').on('click', () => {
            document.body.style.overflow = 'visible';
        });
    }
}

function filterContent(pars = {}) {
}
