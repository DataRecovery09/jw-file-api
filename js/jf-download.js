/**
 * Einzeldateien bekommen den normalen "/serve-file?file=x"-link zur angeforderten Datei.
 * Mehrere Dateien werden auf den POST-Request hin als temporäre zip-Datei vorbereitet. Diese Zip-Datei bleibt
 * für X Sekunden erhalten und kann über einen normalen GET-Link abgerufen werden; nach Ablauf der noch zu
 * ermittelnden Frist wird sie gelöscht.
 * 
 * Die erforderlichen Parameter bekommt JS als json geliefert. Dabei ist [0] die Art der Datei, [1] der Name und
 * [3] der eigentliche Link zum Download.
 */

jQuery(document).ready(() => {
    jQuery('#btn-download-files').click((e) => {
        let package = {};
        package['files'] = jMarkedtiles;
        // console.log(package);
        jQuery.ajax({
            url: jfroot + "/jf-file-download",
            type: 'POST',
            data: package,
            success: function(response) {
                console.log(response)
                let get = response
                let a = document.createElement('a')
                a.style.display = 'none'
                a.href = jfroot + decodeURIComponent(get['link'])
                a.download = response['name']
                document.body.appendChild(a)
                a.click()
                document.body.removeChild(a)
            },
            error: function(response) {
                console.log(response)
            }
        })
    })
});

let jfDownloadFiles = function() {
    console.log("stuff")
}
