<?php
    function uploadFiles($root, $path, $files) {
        if($_SERVER['REQUEST_METHOD'] !== 'POST') {
            throw new Exception("Bitte nur POST verwenden.", 1);
        }
        if(!is_array($files)) {
            throw new Exception("Fehlerhafter Upload.", 1);
        }
        if(count($files) < 1) {
            throw new Exception("Es wurden keine Dateien an den Server übergeben.", 1);
        }
        
        $path = sanitizePath($path);
        $path = cutPathPrefix($path);
        $wholepath = cutSlashes($root . $path);

        $err = array();
        foreach ($files AS $file) {
            var_dump(getMaximumFileUploadSize());
            if(array_key_exists('extension', pathinfo($file['name']))) {
                if(!checkExtension(pathinfo($file['name'])['extension'])) {
                    continue;
                }
                if(!is_uploaded_file($file['tmp_name'])) {
                    continue;
                }
                $targetpath = $wholepath . $file['name'];
                if(file_exists($targetpath)) {
                    $err[$file['name']] = "Eine Datei mit diesem Namen existiert bereits!";
                }
                if(!move_uploaded_file($file['tmp_name'], $targetpath)) {
                    $err[] = $file['name'];
                }
            }
        }
        return $err;
    }



    function checkFileName($filename) {
        return (bool) ((preg_match("`^[-0-9A-Z_\.]+$`i",$filename)) ? true : false);
    }

    function checkExtension($fileext) {
        $dictpath = __DIR__ . "\\..\\" . "fadictionary.json";
        $ext = json_decode(file_get_contents($dictpath), true);
        $iswhitelisted = false;

        foreach($ext AS $entry) {
            if(is_array($entry)) {
                foreach($entry as $val) {
                    if(is_array($val)) {
                        if(in_array($fileext, $val)) {
                            $iswhitelisted = true;
                            break;
                        }
                    }
                }
            }
        }
        // var_dump($iswhitelisted);
        return $iswhitelisted;
    }

    // function checkFileLength($filename) {
    //     return (bool) ((mb_strlen($filename,"UTF-8") > 225) ? true : false);
    // }
?>