<?php
const IMGEXT = ['tif', 'tiff', 'jpg', 'jpeg', 'png', 'bmp', 'gif', 'webp'];
const DOCEXT = ['pdf', 'txt', 'rtf', 'csv'];
const ARCEXT = ['zip', 'rar', '7z'];

function generateFileView($filepath) {
    $ext = "";
    if(array_key_exists("extension", pathinfo($filepath))) {
        $ext = pathinfo($filepath)['extension'];
    }
    $targetjson = urldecode(str_replace(pathinfo($filepath)['extension'], "json", $filepath));
    $targetjson = urldecode($filepath . ".json");

    echo $targetjson . "<br/>";
    $filedata = \file_get_contents($targetjson);

    $fileurl = str_replace("", "/wfv-info", str_replace('rsfile', 'serve-file', $_SERVER['REQUEST_URI']));

    $fileprev = "";

    if(in_array($ext, IMGEXT)) {
        $fileprev = "<img class='resultimg' src='" . $fileurl . "' />";
    } elseif (in_array($ext, DOCEXT)) {
        $fileprev = '<iframe height="800" width="100%" frameborder="0" src="' . $fileurl . '">
                        <p>Insert your error message here, if the PDF cannot be displayed.</p>
                    </iframe>';
    } elseif (in_array($ext, ARCEXT)) {
        $fileprev = "<div style='border-style: solid; border-color: blue;'>Die Datei kann leider nicht im Browser angezeigt werden. Möchten Sie sie herunterladen?</div>";
    } else {
        $fileprev = "<div style='border-style: solid; border-color: blue;'>Die Datei kann leider nicht im Browser angezeigt werden. Möchten Sie sie herunterladen?</div>";
    }

    ?>
    <!DOCTYPE html>
    <html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <link href="/grav-wfvinfo/user/data/gantry5/themes/g5_hydrogen/css-compiled/results.css" type="text/css" rel="stylesheet">
        <title>Blob</title>
    </head>
    <body>
        
    </body>
        <div>
            <h3><?= $filepath ?></h3>
            <?= $fileprev ?>
            <pre><?= var_dump($filedata); ?></pre>
        </div>
    </html>
    <?php
}
?>
