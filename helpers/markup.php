<?php
/**
 * This is essentially the framework markup.
 */
function makeMarkup($root, $path) {
    ?>
    <div id='content-controls' class='content-control-container'>
        <div id='content-tools' class='content-control-element'>
            <form enctype="multipart/form-data" action="/<?php echo $root; ?>/jw-file-upload" method="POST">
                <input type="file" id="input-upload" name="files[]" multiple="true" />
                <label for="input-upload" class="button" id="btn-upload">Hochladen</label>
                <input id="upload-submit" type="submit" value="Dateien hochladen" name="submit" />
            </form>
            <button id='btn-newfolder' class='button' click='doStuff()' title="Neuen Ordner erstellen"><i class='fa fa-folder-o'></i>Neuer Ordner</button>
        </div>
        <!-- <div id='content-sort' class='content-control-element'>
            <label>Sortieren
                <select id='content-sort-select' name='content-sort-select'>
                    <option value='name'>Name</option>
                    <option value='type'>Typ</option>
                    <option value='date-created'>Datum (erstellt)</option>
                    <option value='date-modified'>Datum (verändert)</option>
                    <option value='favs'>Favorisiert</option>
                </select>
            </label>
        </div> -->
        <!-- <div id='content-search' class='content-control-element'>
            <div id='content-search-name' class='content-search-wrapper'></div>
            <div id='content-search-tags' class='content-search-wrapper'></div>
            <div id='content-search-type' class='content-search-wrapper'></div>
            <div id='content-search-favs' class='content-search-wrapper'></div>
            <button id='btn-usefilter' class='button'>Filtern</button>
        </div> -->
        <div id='content-marked-controls' class='content-control-element'>
            <label id="btn-select-all" class="tile-marking"><input type="checkbox" name="marked" value="Markiert" /><span class="checkmark" title="Alle auswählen"></span></label>
            <button id="btn-download-files" class="button button-xsmall" titlw="Ausgewählte Dateien Herunterladen">Download</button>
            <button id="btn-delete" class="button button-xsmall red" title="Ausgewählte Löschen"><i class="fa fa-trash" aria-hidden="true"></i></button>
        </div>
    </div>
    <div class="seg-container">
        <div id="treeview-container"></div>
        <div id='ajax-results' class='result-container'></div>
    </div>
    <script>
        const jfroot = "<?= $root ?>";
        const jfMaxFileSize = <?php echo getMaximumFileUploadSize(); ?>;
        function formatBytes(a,b){if(0==a)return"0 Bytes";var c=1024,d=b||2,e=["Bytes","KB","MB","GB","TB","PB","EB","ZB","YB"],f=Math.floor(Math.log(a)/Math.log(c));return parseFloat((a/Math.pow(c,f)).toFixed(d))+" "+e[f]};
        jFileSystem('<?= $path ?>', '<?= $root ?>');
    </script>
    <?php
}

function addModalOverlay($root) {
    ?>
    <div id="jf-modal-overlay" class="jf-modal">
        <div id="jf-modal-content" class="jf-modal-content">
            <div id="jf-modal-head"><h3>MODAL HEAD</h3></div>
            <div id="jf-modal-main">Lipsum dolor sit amet consectetur adipisicing elit. Unde rerum quas rem necessitatibus explicabo ea molestias maxime voluptatibus odit quis minima quidem sed voluptas sit autem veritatis exercitationem, recusandae facilis!</div>
            <div id="jf-modal-foot">MODAL FOOT
                <div class="jf-modal-btn-container">
                    <button id="jf-modal-btn-0" class="btn jf-modal-hidden">Button0</button>
                    <button id="jf-modal-btn-1" class="btn jf-modal-hidden">Button1</button>
                </div>
            </div>
        </div>
    </div>
    <?php
}

?>
