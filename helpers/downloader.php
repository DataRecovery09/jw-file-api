<?php
/**
 * Einzeldateien bekommen den normalen "/serve-file?file=x"-link zur angeforderten Datei.
 * Mehrere Dateien werden auf den POST-Request hin als temporäre zip-Datei vorbereitet. Diese Zip-Datei bleibt
 * für X Sekunden erhalten und kann über einen normalen GET-Link abgerufen werden; nach Ablauf der noch zu
 * ermittelnden Frist wird sie gelöscht.
 * 
 * Die erforderlichen Parameter bekommt JS als json geliefert. Dabei ist ['type'] der Dateityp, ['name'] der Name und
 * ['link'] der eigentliche Link zum Download.
 */

function downloadFiles($root, $files, $userh) {
    $realpaths = array();
    $json = array();

    // Check, if we have to pack the files into a zip. This is NOT the case if the user
    // just wants to download a single entry/file that is not a directory.
    if(count($files) < 2 && isset(pathinfo($files[0])['extension'])) {
        $path = cutSlashes(cutPathPrefix(sanitizePath(\rawurldecode($files[0]))));
        $rawpath = cutSlashes($root . $path);
        $file = substr_replace($rawpath ,"", -1);

        if(file_exists($file)) {
            $link = rawurlencode(rtrim("/serve-file?file=" . $path, '/\\'));

            $json['type'] = mime_content_type($file);
            $json['name'] = pathinfo($file)['basename'];
            $json['link'] = $link;

            header('Content-Type: application/json');
            echo json_encode($json);
            exit();
        }
    }

    foreach($_POST['files'] AS $path) {
        $path = cutSlashes(cutPathPrefix(sanitizePath(\rawurldecode($path))));
        $discpath = cutSlashes($root . $path);
        if(!is_dir($discpath)) {
            $realpaths[$path] = rtrim($discpath, '/\\');
        }
        if(realpath($discpath)) {
            $realpaths[$path] = $discpath;
        }
    }

    $res = array();
    foreach ($realpaths as $path => $discpath) {
        if(is_dir($discpath)) {
            $res = array_merge($res, createZipPathCompendium($discpath, $path));
        } else {
            $res[rtrim($path, '/\\')] = $discpath;
        }
    }


    $zip = new ZipArchive;
    $resziplink = '_tmp/' . $userh . "_archive_0.zip";
    $resziplink = '_tmp/' . $userh . "_archive_0.zip";
    $resziplink = recursiveZipExists($root, $resziplink);
    $absziplink = $root . $resziplink;
    // var_dump($absziplink);



    if($zip->open($absziplink, ZipArchive::CREATE) === true) {
        foreach($res AS $key => $value) {
            $zip->addFile($value, ltrim($key, '/\\'));
        }    
        $zip->close();
    }

    $path = cutSlashes(cutPathPrefix(sanitizePath(\rawurldecode($files[0]))));
    $link = rawurlencode(rtrim("/serve-file?file=/" . $resziplink, '/\\'));
    
    $json['type'] = mime_content_type($absziplink);
    $json['name'] = pathinfo($absziplink)['basename'];
    $json['link'] = $link;

    header('Content-Type: application/json');
    echo json_encode($json);
    
    // sleep(600);
    // unlink($absziplink);
    exit();
}


/**
 * RECURSIVE FUNCTION
 * Gives back an array of 'abspath' => 'relpath', where 'abspath' is the absolute path of the file
 * on the harddrive and 'relpath' will be used as the path of the file in the zip archive.
 */
function createZipPathCompendium($rootpath, $zippath) {
    $arr = array();
    if(is_dir($rootpath)) {
        $toplevel = array_diff(scandir($rootpath), array(".", ".."));
        if(count($toplevel) === 0) {
            return $arr;
        }
        foreach($toplevel AS $entry) {
            if(isset(pathinfo($entry)['extension'])) {
                if(pathinfo($entry)['extension'] === "json") {
                    continue;
                }
            }
            if(is_dir($rootpath . $entry)) {
                $res = createZipPathCompendium(cutSlashes($rootpath . "/" . $entry . "/"), cutSlashes("/" . $zippath . "/" . $entry . "/"));
                $arr = array_merge($arr, $res);
            } else {
                $arr[$zippath . $entry] = $rootpath . $entry;
            }
        }
    }
    return $arr;
}

// This is ugly as hell. If you know a better solution, PLEASE tell me!
function recursiveZipExists($root, $path) {
    if(file_exists($root . $path)) {
        $patharr = explode("_", $path);
        $endarr = explode(".", $patharr[array_key_last($patharr)]);
        $currentnr = intval($endarr[0]);
        $currentnr++;
        $endarr[0] = strval($currentnr);
        $endval = implode(".", $endarr);
        $patharr[array_key_last($patharr)] = $endval;
        $newpath = implode("_", $patharr);
        $path = recursiveZipExists($root, $newpath);
    }
    return $path;
}

function readfile_chunked($filename, $retbytes = true) {
    $chunksize = 1*(1024*1024); // how many bytes per chunk
    $buffer = '';
    $cnt =0;
    $handle = fopen($filename, 'rb');
    if ($handle === false) {
        return false;
    }
    while (!feof($handle)) {
        $buffer = fread($handle, $chunksize);
        echo $buffer;
        ob_flush();
        flush();
        if ($retbytes) {
            $cnt += strlen($buffer);
        }
    }
        $status = fclose($handle);
    if ($retbytes && $status) {
        return $cnt; // return num. bytes delivered like readfile() does.
    }
    return $status;
 
}


function download($filePath) {
    set_time_limit(0); 
    ini_set('memory_limit', '512M');

    if(!empty($filePath)) 
    { 
        $fileInfo = pathinfo($filePath);
        $fileName  = $fileInfo['basename'];
        $fileExtension   = $fileInfo['extension']; 
        $default_contentType = "application/octet-stream"; 
        $content_types_list = mimeTypes(); 
        // to find and use specific content type, check out this IANA page : http://www.iana.org/assignments/media-types/media-types.xhtml 
        if (array_key_exists($fileExtension, $content_types_list))  
        { 
            $contentType = $content_types_list[$fileExtension];
        } 
        else 
        { 
            $contentType =  $default_contentType; 
        } 
        if(file_exists($filePath)) 
        { 
            $size = filesize($filePath);
            $offset = 0; 
            $length = $size; 
            //HEADERS FOR PARTIAL DOWNLOAD FACILITY BEGINS 
            if(isset($_SERVER['HTTP_RANGE'])) 
            { 
                preg_match('/bytes=(\d+)-(\d+)?/', $_SERVER['HTTP_RANGE'], $matches); 
                $offset = intval($matches[1]);
                $length = intval($matches[2]) - $offset; 
                $fhandle = fopen($filePath, 'r'); 
                fseek($fhandle, $offset); // seek to the requested offset, this is 0 if it's not a partial content request 
                $data = fread($fhandle, $length); 
                fclose($fhandle); 
                header('HTTP/1.1 206 Partial Content'); 
                header('Content-Range: bytes ' . $offset . '-' . ($offset + $length) . '/' . $size); 
            }//HEADERS FOR PARTIAL DOWNLOAD FACILITY BEGINS 
            //USUAL HEADERS FOR DOWNLOAD

            header("Content-Disposition: attachment;filename=".$fileName); 
            header('Content-Type: '.$contentType); 
            header("Accept-Ranges: bytes"); 
            header("Pragma: public"); 
            header("Expires: -1"); 
            header("Cache-Control: no-cache"); 
            header("Cache-Control: public, must-revalidate, post-check=0, pre-check=0"); 
            header("Content-Length: ".filesize($filePath)); 
            
            $chunksize = 8 * (1024 * 1024); //8MB (highest possible fread length) 
            if ($size > $chunksize) 
            { 
                $handle = fopen($filePath, 'r');
                // var_dump($handle);
                // exit();
                // $handle = fopen($_FILES["file"]["tmp_name"], 'rb');
              $buffer = ''; 
              while (!feof($handle) && (connection_status() === CONNECTION_NORMAL))  
              { 
                $buffer = fread($handle, $chunksize); 
                print $buffer; 
                ob_flush(); 
                flush();
                
                usleep(200);
              } 
              if(connection_status() !== CONNECTION_NORMAL) 
              { 
                echo "Connection aborted"; 
              } 
              fclose($handle); 
            } 
            else  
            { 
              ob_clean(); 
              flush(); 
              readfile($filePath); 
            } 
         } 
         else 
         { 
           echo 'File does not exist!'; 
         } 
    } 
    else 
    { 
        echo 'There is no file to download!'; 
    } 
}     


/* Function to get correct MIME type for download */ 
function mimeTypes() {
    /* Just add any required MIME type if you are going to download something not listed here.*/ 
    $mime_types = array("323" => "text/h323", 
                        "acx" => "application/internet-property-stream", 
                        "ai" => "application/postscript", 
                        "aif" => "audio/x-aiff", 
                        "aifc" => "audio/x-aiff", 
                        "aiff" => "audio/x-aiff", 
                        "asf" => "video/x-ms-asf", 
                        "asr" => "video/x-ms-asf", 
                        "asx" => "video/x-ms-asf", 
                        "au" => "audio/basic", 
                        "avi" => "video/x-msvideo", 
                        "axs" => "application/olescript", 
                        "bas" => "text/plain", 
                        "bcpio" => "application/x-bcpio", 
                        "bin" => "application/octet-stream", 
                        "bmp" => "image/bmp", 
                        "c" => "text/plain", 
                        "cat" => "application/vnd.ms-pkiseccat", 
                        "cdf" => "application/x-cdf", 
                        "cer" => "application/x-x509-ca-cert", 
                        "class" => "application/octet-stream", 
                        "clp" => "application/x-msclip", 
                        "cmx" => "image/x-cmx", 
                        "cod" => "image/cis-cod", 
                        "cpio" => "application/x-cpio", 
                        "crd" => "application/x-mscardfile", 
                        "crl" => "application/pkix-crl", 
                        "crt" => "application/x-x509-ca-cert", 
                        "csh" => "application/x-csh", 
                        "css" => "text/css", 
                        "dcr" => "application/x-director", 
                        "der" => "application/x-x509-ca-cert", 
                        "dir" => "application/x-director", 
                        "dll" => "application/x-msdownload", 
                        "dms" => "application/octet-stream", 
                        "doc" => "application/msword", 
                        "dot" => "application/msword", 
                        "dvi" => "application/x-dvi", 
                        "dxr" => "application/x-director", 
                        "eps" => "application/postscript", 
                        "etx" => "text/x-setext", 
                        "evy" => "application/envoy", 
                        "exe" => "application/octet-stream", 
                        "fif" => "application/fractals", 
                        "flr" => "x-world/x-vrml", 
                        "gif" => "image/gif", 
                        "gtar" => "application/x-gtar", 
                        "gz" => "application/x-gzip", 
                        "h" => "text/plain", 
                        "hdf" => "application/x-hdf", 
                        "hlp" => "application/winhlp", 
                        "hqx" => "application/mac-binhex40", 
                        "hta" => "application/hta", 
                        "htc" => "text/x-component", 
                        "htm" => "text/html", 
                        "html" => "text/html", 
                        "htt" => "text/webviewhtml", 
                        "ico" => "image/x-icon", 
                        "ief" => "image/ief", 
                        "iii" => "application/x-iphone", 
                        "ins" => "application/x-internet-signup", 
                        "isp" => "application/x-internet-signup", 
                        "jfif" => "image/pipeg", 
                        "jpe" => "image/jpeg", 
                        "jpeg" => "image/jpeg", 
                        "jpg" => "image/jpeg", 
                        "js" => "application/x-javascript", 
                        "latex" => "application/x-latex", 
                        "lha" => "application/octet-stream", 
                        "lsf" => "video/x-la-asf", 
                        "lsx" => "video/x-la-asf", 
                        "lzh" => "application/octet-stream", 
                        "m13" => "application/x-msmediaview", 
                        "m14" => "application/x-msmediaview", 
                        "m3u" => "audio/x-mpegurl", 
                        "man" => "application/x-troff-man", 
                        "mdb" => "application/x-msaccess", 
                        "me" => "application/x-troff-me", 
                        "mht" => "message/rfc822", 
                        "mhtml" => "message/rfc822", 
                        "mid" => "audio/mid", 
                        "mny" => "application/x-msmoney", 
                        "mov" => "video/quicktime", 
                        "movie" => "video/x-sgi-movie", 
                        "mp2" => "video/mpeg", 
                        "mp3" => "audio/mpeg", 
                        "mpa" => "video/mpeg", 
                        "mpe" => "video/mpeg", 
                        "mpeg" => "video/mpeg", 
                        "mpg" => "video/mpeg", 
                        "mpp" => "application/vnd.ms-project", 
                        "mpv2" => "video/mpeg", 
                        "ms" => "application/x-troff-ms", 
                        "mvb" => "application/x-msmediaview", 
                        "nws" => "message/rfc822", 
                        "oda" => "application/oda", 
                        "p10" => "application/pkcs10", 
                        "p12" => "application/x-pkcs12", 
                        "p7b" => "application/x-pkcs7-certificates", 
                        "p7c" => "application/x-pkcs7-mime", 
                        "p7m" => "application/x-pkcs7-mime", 
                        "p7r" => "application/x-pkcs7-certreqresp", 
                        "p7s" => "application/x-pkcs7-signature", 
                        "pbm" => "image/x-portable-bitmap", 
                        "pdf" => "application/pdf", 
                        "pfx" => "application/x-pkcs12", 
                        "pgm" => "image/x-portable-graymap", 
                        "pko" => "application/ynd.ms-pkipko", 
                        "pma" => "application/x-perfmon", 
                        "pmc" => "application/x-perfmon", 
                        "pml" => "application/x-perfmon", 
                        "pmr" => "application/x-perfmon", 
                        "pmw" => "application/x-perfmon", 
                        "pnm" => "image/x-portable-anymap", 
                        "pot" => "application/vnd.ms-powerpoint", 
                        "ppm" => "image/x-portable-pixmap", 
                        "pps" => "application/vnd.ms-powerpoint", 
                        "ppt" => "application/vnd.ms-powerpoint", 
                        "prf" => "application/pics-rules", 
                        "ps" => "application/postscript", 
                        "pub" => "application/x-mspublisher", 
                        "qt" => "video/quicktime", 
                        "ra" => "audio/x-pn-realaudio", 
                        "ram" => "audio/x-pn-realaudio", 
                        "ras" => "image/x-cmu-raster", 
                        "rgb" => "image/x-rgb", 
                        "rmi" => "audio/mid", 
                        "roff" => "application/x-troff", 
                        "rtf" => "application/rtf", 
                        "rtx" => "text/richtext", 
                        "scd" => "application/x-msschedule", 
                        "sct" => "text/scriptlet", 
                        "setpay" => "application/set-payment-initiation", 
                        "setreg" => "application/set-registration-initiation", 
                        "sh" => "application/x-sh", 
                        "shar" => "application/x-shar", 
                        "sit" => "application/x-stuffit", 
                        "snd" => "audio/basic", 
                        "spc" => "application/x-pkcs7-certificates", 
                        "spl" => "application/futuresplash", 
                        "src" => "application/x-wais-source", 
                        "sst" => "application/vnd.ms-pkicertstore", 
                        "stl" => "application/vnd.ms-pkistl", 
                        "stm" => "text/html", 
                        "svg" => "image/svg+xml", 
                        "sv4cpio" => "application/x-sv4cpio", 
                        "sv4crc" => "application/x-sv4crc", 
                        "t" => "application/x-troff", 
                        "tar" => "application/x-tar", 
                        "tcl" => "application/x-tcl", 
                        "tex" => "application/x-tex", 
                        "texi" => "application/x-texinfo", 
                        "texinfo" => "application/x-texinfo", 
                        "tgz" => "application/x-compressed", 
                        "tif" => "image/tiff", 
                        "tiff" => "image/tiff", 
                        "tr" => "application/x-troff", 
                        "trm" => "application/x-msterminal", 
                        "tsv" => "text/tab-separated-values", 
                        "txt" => "text/plain", 
                        "uls" => "text/iuls", 
                        "ustar" => "application/x-ustar", 
                        "vcf" => "text/x-vcard", 
                        "vrml" => "x-world/x-vrml", 
                        "wav" => "audio/x-wav", 
                        "wcm" => "application/vnd.ms-works", 
                        "wdb" => "application/vnd.ms-works", 
                        "wks" => "application/vnd.ms-works", 
                        "wmf" => "application/x-msmetafile", 
                        "wps" => "application/vnd.ms-works", 
                        "wri" => "application/x-mswrite", 
                        "wrl" => "x-world/x-vrml", 
                        "wrz" => "x-world/x-vrml", 
                        "xaf" => "x-world/x-vrml", 
                        "xbm" => "image/x-xbitmap", 
                        "xla" => "application/vnd.ms-excel", 
                        "xlc" => "application/vnd.ms-excel", 
                        "xlm" => "application/vnd.ms-excel", 
                        "xls" => "application/vnd.ms-excel", 
                        "xlt" => "application/vnd.ms-excel", 
                        "xlw" => "application/vnd.ms-excel", 
                        "xof" => "x-world/x-vrml", 
                        "xpm" => "image/x-xpixmap", 
                        "xwd" => "image/x-xwindowdump", 
                        "z" => "application/x-compress", 
                        "rar" => "application/x-rar-compressed", 
                        "zip" => "application/zip"); 
    return $mime_types;                     
}

?>
