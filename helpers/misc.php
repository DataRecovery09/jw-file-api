<?php
function cutPathPrefix($path) {
    $path = "/" . $path . "/"; // Don't ask...
    $path = \str_replace("/files/", "", $path);
    return $path;
}
function cutSlashes($path) {
    $path = preg_replace('#/+#', '/', $path);
    return $path;
}

function sanitizePath($path) {
    // $log = array();
    // $log[] = $path;

    $path = str_replace('\\', '/', $path);
    // $log[] = $path;
    $path = preg_replace('/\/+/', '/', $path);
    // $log[] = $path;
    $path = str_replace("../", "/", $path);
    // $log[] = $path;
    $path = str_replace("./", "/", $path);
    // $log[] = $path;
    $path = cutSlashes($path);
    // $log[] = $path;
    $path = strip_tags($path);
    // $log[] = $path;

    return $path;
}




/**
* This function returns the maximum files size that can be uploaded 
* in PHP
* @returns int File size in bytes
**/
function getMaximumFileUploadSize()  
{  
    return min(convertPHPSizeToBytes(ini_get('post_max_size')), convertPHPSizeToBytes(ini_get('upload_max_filesize')));  
}  

/**
* This function transforms the php.ini notation for numbers (like '2M') to an integer (2*1024*1024 in this case)
* 
* @param string $sSize
* @return integer The value in bytes
*/
function convertPHPSizeToBytes($sSize)
{
    //
    $sSuffix = strtoupper(substr($sSize, -1));
    if (!in_array($sSuffix,array('P','T','G','M','K'))){
        return (int)$sSize;  
    } 
    $iValue = substr($sSize, 0, -1);
    switch ($sSuffix) {
        case 'P':
            $iValue *= 1024;
            // Fallthrough intended
        case 'T':
            $iValue *= 1024;
            // Fallthrough intended
        case 'G':
            $iValue *= 1024;
            // Fallthrough intended
        case 'M':
            $iValue *= 1024;
            // Fallthrough intended
        case 'K':
            $iValue *= 1024;
            break;
    }
    return (int)$iValue;
}


// Thanks to Simolation! Found it here:
// https://codesnipps.simolation.com/post/php/create-thumbnails-on-the-fly/
function createThumbnail($path, $width = 150, $height = 150, $quality = 85) {
    try {
        switch (pathinfo($path)['extension']) {
            case 'jpg':
                $image = \imagecreatefromjpeg($path);
                break;
            case 'jpeg':
                $image = \imagecreatefromjpeg($path);
                break;
            case 'png':
                $image = \imagecreatefrompng($path);
                break;
            case 'webp':
                $image = \imagecreatefromwebp($path);
                break;
            case 'bmp':
                $image = \imagecreatefrombmp($path);
                break;
            case 'gif':
                $image = \imagecreatefromgif($path);
                break;
            default:
                header('HTTP/1.0 400 Bad Request');
                throw new Exception("Error Processing Request", 1);
                break;
        }
        $w = imagesx($image);
        $h = imagesy($image);

        $create = imagecreatetruecolor($width, $height); 
        imagecopyresized($create, $image, 0, 0, 0, 0, $width, $height, $w, $h);
        imagejpeg($create, null, $quality);
        return $create;
    } catch (\Exception $e) {
        echo ($e->getMessage());
        exit();
    }
}


?>